﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Collections.Generic;
using System.Diagnostics;

namespace lab3Testing.Tests
{
    [TestClass]
    public class MyStackTests
    {
        private MyStack<Carriage> train;
        private Carriage carriage;

        //запускается перед запуском каждого тестируемого метода
        // Есть еще ClassInitialize - он запускается 1 раз перед первым тестом
        [TestInitialize]
        public void TestInitialize()
        {
            Debug.WriteLine("Тест проинициализирован");
            //поезд из 10 вагонов
            train = new MyStack<Carriage>();
            //создание вагона
            carriage = new Carriage("в1", false);
        }


        //------------------------
        // -- MyStack<T>.IsEmpty --
        //------------------------
        [TestMethod]
        public void IsEmpty_ReturnTrue()
        {
            bool expected = true;
            bool actual = train.IsEmpty;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsEmpty_ReturnFalse()
        {
            bool expected = false;
            train.Push(carriage);
            bool actual = train.IsEmpty;
            Assert.AreEqual(expected, actual);
        }
        //------------------------

 
        //------------------------
        // -- MyStack<T>.Count --
        //------------------------
        [TestMethod]
        public void Count_Return0()
        {
            int expected = 0;
            int actual = train.Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Count_Return1()
        {
            int expected = 1;
            train.Push(carriage);
            int actual = train.Count;
            Assert.AreEqual(expected, actual);
        }
        //------------------------


        //------------------------
        // -- MyStack<T>.Push(T item) --
        //------------------------
        [TestMethod]
        public void Push_1carriage_Return1()
        {
            //arrange
            int expected = 1;
            //act
            int actual = train.Push(carriage);
            //assert
            Assert.AreEqual(expected,actual, "В поезд добавился 1 вагон");
        }

        [TestMethod]
        public void Push_10carriages_Return10()
        {
            int expected = 10;
            int actual = -1;

            for (int i = 0; i < 10; i++)
            {
                actual = train.Push(carriage);
            }

            Assert.AreEqual(expected, actual, "В поезд добавились 10 вагонов (максимальное значение)");
        }

        [ExpectedException(typeof(InvalidOperationException), "Исключение не было выброшено")]
        [TestMethod]
        public void Push_11carriages_ReturnException()
        {
            for (int i = 0; i < 11; i++)
            {
                train.Push(carriage);
            }
        }
        //------------------------


        //------------------------
        // -- MyStack<T>.Pop() --
        //------------------------
        //ExpectedException - тест будет успешным, если в процессе выполнения
        //будет выброшено исключение
        [ExpectedException(typeof(InvalidOperationException), "Исключение не было выброшено")]
        [TestMethod]
        public void Pop_EmptyStack_Exception()
        {
            train.Pop();
        }

        [TestMethod]
        public void Pop_Stack1Carriage_ReturnCarriage()
        {
            Carriage expected = carriage;

            train.Push(carriage);
            Debug.WriteLine("Количество элементов до Pop: " + train.Count);
            Carriage actual = train.Pop();
            Debug.WriteLine("Количество элементов после Pop: " + train.Count);

            Assert.AreEqual(expected, actual);
        }
        //------------------------


        //------------------------
        // -- MyStack<T>.Peek() --
        //------------------------
        [ExpectedException(typeof(InvalidOperationException), "Исключение не было выброшено")]
        [TestMethod]
        public void Peek_EmptyStack_Exception()
        {
            train.Peek();
        }

        [TestMethod]
        public void Peek_Stack1Carriage_ReturnCarriage()
        {
            Carriage expected = carriage;

            train.Push(carriage);
            Debug.WriteLine("Количество элементов до Peek: " + train.Count);
            Carriage actual = train.Peek();
            Debug.WriteLine("Количество элементов после Peek: " + train.Count);

            Assert.AreEqual(expected, actual);
        }
        //------------------------


        //------------------------
        // -- MyStack<T>.ToString() --
        //------------------------
        [TestMethod]
        public void ToString_CorrectStringReturn()
        {
            train.Push(carriage);
            StringAssert.Equals(train.ToString(), "Вагон: в1 ТИП: 0\n");
        }
        //------------------------


        //запускается после завершения каждого тестируемого метода
        [TestCleanup]
        public void TestCleanUp()
        {
            Debug.WriteLine("Очистка теста");
            train = null;
        }
    }
}
