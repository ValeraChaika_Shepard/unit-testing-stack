﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab3Testing
{
    class TShapedSortUnit
    {
        private MyStack<Carriage> train;  
        private List<Carriage> westernDirection;
        private List<Carriage> eastDirection;

        public TShapedSortUnit(MyStack<Carriage> train)
        {
            westernDirection = new List<Carriage>();
            eastDirection = new List<Carriage>();
            this.train = train;
        }

        public void SortTrain()
        {
            for(int i = train.Count - 1; i >= 0; i--)
            {
                Carriage carriage = train.Pop();
                if (carriage.GetCarriageType() == false)
                    westernDirection.Add(carriage);
                else
                    eastDirection.Add(carriage);
            }
        }

        public List<Carriage> GetCarriagesFromDirection(bool direction)
        {
            if (direction == false)
                return westernDirection;
            else
                return eastDirection;
        }

        public MyStack<Carriage> GetDirectionTrain(bool direction)
        {
            int amountOfCarriages;
            if (direction == false)
                amountOfCarriages = westernDirection.Count;
            else
                amountOfCarriages = eastDirection.Count;

            MyStack<Carriage> dtrain = new MyStack<Carriage>(amountOfCarriages);

            for(int i = 0; i < amountOfCarriages; i++)
            {
                if (direction == false)
                    dtrain.Push(westernDirection[i]);
                else
                    dtrain.Push(eastDirection[i]);
            }
            return dtrain;
        }
    }
}
