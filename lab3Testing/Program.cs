﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab3Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество вагонов в составе: ");
            int amountCarriages = Convert.ToInt32(Console.ReadLine());

            MyStack<Carriage> train = new MyStack<Carriage>(amountCarriages);

            for(int i = 0; i < amountCarriages; i++)
            {
                Console.Write("Введите тип(0/1) для вагона ВА" + (i + 1) + ": ");
                int intType = Convert.ToInt32(Console.ReadLine());
                Carriage tmp = new Carriage("ВА" + (i + 1), Convert.ToBoolean(intType));
                train.Push(tmp);
            }

            Console.WriteLine("\nСостав: ");
            Console.WriteLine(train.ToString());

            TShapedSortUnit tshape = new TShapedSortUnit(train);
            tshape.SortTrain();
            List<Carriage> wd = tshape.GetCarriagesFromDirection(false);
            List<Carriage> ed = tshape.GetCarriagesFromDirection(true);

            Console.WriteLine("Вагоны западного направления(0): ");
            for (int i = 0; i < wd.Count; i++)
            {
                Console.WriteLine(wd[i].ToString());
            }

            Console.WriteLine("\nВагоны восточного направления(1): ");
            for (int i = 0; i < ed.Count; i++)
            {
                Console.WriteLine(ed[i].ToString());
            }

            Console.WriteLine("\nПоезд западного направления(0): ");
            Console.WriteLine(tshape.GetDirectionTrain(false).ToString());

            Console.WriteLine("Поезд восточного направления(1): ");
            Console.WriteLine(tshape.GetDirectionTrain(true).ToString());

            Console.ReadKey();
        }
    }
}
