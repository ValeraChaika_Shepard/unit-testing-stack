﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab3Testing
{
    public class Carriage
    {
        private string nameNumber;
        private bool type;

        public Carriage(string nameNumber, bool type)
        {
            this.nameNumber = nameNumber;
            this.type = type;
        }

        public override string ToString()
        {
            int intType = Convert.ToInt32(type);
            return "Вагон: " + nameNumber + " ТИП: " + intType;
        }

        public bool GetCarriageType()
        {
            return type;
        }
    }
}
