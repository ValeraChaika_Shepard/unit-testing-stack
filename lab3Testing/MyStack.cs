﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab3Testing
{
    public class MyStack<T>
    {
        private T[] items; // элементы стека
        private int count;  // количество элементов (на текущий момент)
        const int n = 10;   // количество элементов в массиве по умолчанию

        public MyStack()
        {
            items = new T[n];
        }

        public MyStack(int length)
        {
            items = new T[length];
        }
        
        // пуст ли стек
        public bool IsEmpty
        {
            get { return count == 0; }
        }
        
        // размер стека
        public int Count
        {
            get { return count; }
        }
        
        // добвление элемента
        public int Push(T item)
        {
            // если стек заполнен, выбрасываем исключение
            if (count == items.Length)
                throw new InvalidOperationException("Переполнение стека");
            items[count++] = item;
            return Count;
        }
        
        // извлечение элемента
        public T Pop()
        {
            // если стек пуст, выбрасываем исключение
            if (IsEmpty)
                throw new InvalidOperationException("Стек пуст");
            T item = items[--count];
            items[count] = default(T); // сбрасываем ссылку
            return item;
        }

        // возвращаем элемент из верхушки стека
        public T Peek()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Стек пуст");
            return items[count - 1];
        }

        public override string ToString()
        {
            string res = "";
            for(int i = Count - 1; i >= 0; i--)
            {
                res += items[i].ToString() + "\n";
            }
            return res;
        }
    }
}
